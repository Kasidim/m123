# M123 ePortfolio

>Geschrieben von Aaron Sigrist

[Return to Main](/README.md)

[TOC]

## Einleitung

Quick Doku zur Installation eines Printservers auf Windows 2022.

Der Server und Client sollte diese Vorgaben erfühlen:

Server

- Statische IP für Bridg Netzwerk (Gleicher wie Drucker)
- Statische IP im M117-Lab Netzwerk

Client

- Statische IP im M117-Lab Netzwerk
- Firewall Einstellungen wie in [DNS Server](/DNS/README.md)
- Client sollte im generelen wie in [DNS Server](/DNS/README.md) Konfiguriert sein.

## Installation Druckdienst

Installieren sie als Erstes Windows Server 2022.
Diese Installation sollte automatisch verlaufen.
Ändern sie die Firewall wie in [DNS Server](/DNS/README.md)

Nach der Installation müssen wir noch den Druckdienst Installieren.

Klicke dafür auf "Add roles and features" und gehe die Installation durch.

![img1](./images/img1.png)

Die Rolle "Print and Document Services" Installieren.

![img2](./images/img2.png)

Nach der Installation können wir mit der Konfiguration beginnen.

Gehe über Tools auf "Print Management".

![img3](./images/img3.png)

Klicke RechtsklickFüge den Printer Hinzu.

![img4](./images/img4.png)

![img5](./images/img5.png)

Nach dem Hinzufügen können sie eine Testseite drucken um zu sehen ob der drucker richtig verbunden ist.

## Drucker auf Clients Hinzufügen

Benutzen sie einen Client den sie schon in vorherigen Modulen Konfiguriert haben.

Gehen sie über den Explorer auf "Network" und suchen sie ihren server.
Evtl müssen sie die IP-Addresse des Servers eingeben.

![img6](./images/img6.png)

Hier sollte die Einrichtung Beendet sein.

## Troubleshooting

Hir werde ich ein Troubleshooting Dokumentieren **OHNE BILDER**.
Was ich alles versucht habe und ob es Funktioniert hat:

- Ich habe diverse Einstellungen am Printserver selbst(Port, Sharing) ausprobiert.
- Ich habe beim Client auch diverse Einstellung(Port, Sharing,) ausprobiert.
- Ich habe die mit einer Policy versucht den dienst richtig zu verbinden.
- Registry Edits beim Client und Server versucht.

Keine dieser Troubeshootings hat Funktioniert. Leider :(
