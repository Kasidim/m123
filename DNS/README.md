# M123 ePortfolio

>Geschrieben von Aaron Sigrist

[Return to Main](/README.md)

[TOC]

## Einleitung

Wir werden in dieser Dokumentation einen DNS-Server auf Windows Server 2022.
Falls man die 2019 version installiert hat sollte dies auch trotzdem Funktionieren.

Ich habe meine Dokumentation/Anleitung durch eine Anleitung im Internet, wie auch eigene Lösungen für bestimmte Konfigurationsschwiergkeiten zusamengestellt.

Ich habe eine Anleitung im Internet gefunden für die aufsetzung.
Diese habe ich dann auch in den Klassenchat gesendet um allen eine einfachere Konfiguration zu ermöglichen.

Anleitungs Link: *<https://computingforgeeks.com/configure-dns-server-on-windows-server-2022/>*

## DNS Installieren

### Vorbereitung

- Haben Sie einen Client mit dem M117 Netzwerkadapter bereit.
- Erstellen und INstallieren sie eine VM für den DNS Server. Dieser soll Folgende Hardwareeigenschaften haben:

![Hardware](./images/win1.png)

Benutzen Sie ihr M117 Netzwerk als Host-Only Netzwerk bei ihrem Netzwerkadapter.
Der Host-Only Netzwerkadapter dient zur erleichterten Konfiguration des Servers.

Wenn alles eingerichtet ist können sie den Windows 2022 Server Installieren.

### Statische IP-Adresse Vergeben

Windows Server sollte sich von selbst installieren

Jetzt müssen sie ihrem Server eine Statische IP-Adresse vergeben.
Sie sollten schon wissen wo sie diese IP-Adresse ändern.
Weswegen ich ihnen nur meine Endkonfiguration der IP-Adresse zeigen werde.
Natürlich müssen sie nicht meine IP-Adressen Übernehmen.

Stellen sie den DNS Bereich wie in diesem Bild ein:

![IP-config](./images/win2.png)

Der Grund weshalb wir dem DNS diese DNS Adressen geben ist:

- 127.0.0.1 für loopback
- 8.8.8.8 für den Google DNS

Speichern Sie diese Einstellung und starten sie ihren Server neu.

### DNS auf Windows-Server Installieren

Öffnen sie das Dashboard im Servermanager. Der sollte sich Automatisch am anfagn geöffnet haben. Falls sie ihn aber den Servermanager geschlossen, suchen sie ihn einfach in der suchleiste.

Klicken sie im Dashboard dann auf "Add roles and features".
Folgen sie dann der Bildanleitung.

| Bild | Erklärung |
| - | - |
| ![InstallAdd](./images/win3.png) | Klicken sie auf "Add roles and fetures" |
| ![Installation01](./images/win4.png) | Ignorieren sie den "Before you Begin" Teil und wählen sie "Role-based or feature-based installation" aus.
 ![Installation02](./images/win5.png) | Wählen sie ihren Server aus. **Info: Falls ihr Server eine APIPA Adresse hat müssen sie ihre Netzwerkeinstellungen überprüfen und den Server neustarten.**
 ![Installation03](./images/win6.png) ![Installation04](./images/win7.png) | Wählen sie die DNS Server-Installation aus. Klicken sie danach einfach auf "Add Features".
 ![Installation05](./images/win8.png) | Ignorieren sie die Features und klicken sie auf Install.
 ![Installation06](./images/win9.png) | Setzen sie einen Hacken beim Kästchen für den Automatieschen Restart.*(Auch wenn er nicht unbedingt neustarten wird.)* Starten sie jetzt die Installation.
 ![Installation07](./images/win10.png) ![Installation08](./images/win11.png) | Wenn die Installation abgeschlossen wurde können sie das Fenster schliessen.

## DNS Konfigurieren

### Forward Lookup Zone Einrichtern

| Bild | Erklärung |
| - | - |
 ![Conf1](./images/win12.png) | Öffnen sie die DNS Konfiguration, um die Lookupzones zu Konfigurieren. *Info: Dies muss gemacht werden damit der DNS überhaupt im Netzwerk Sichtbar ist.*
![Conf2](./images/win13.png) | Rechtsklick auf "Forward Lookup Zone" und "New Zone" anklicken.
 ![Conf3](./images/win14.png) | Wählen sie eine Primary Zone aus
 ![Conf4](./images/win15.png) | Bennenen sie ihre DNS Zone mit ihrem Domain Namen.
 ![conf5](./images/win16.png) | Machen sie ein neues File.
 ![Conf6](./images/win17.png) | Erlauben sie keine Dynamischen updates.

>Jetzt können Sie ihre Einstellungen nochmals Überprüfen und die Konfiguration abschliessen.

### Reverse Lookup Zone Einrichten

| Bilder | Erklärung |
| - | - |
| ![Conf7](./images/win23.png) |Rechtsklick auf "Reverse Lookup Zone" und dan auf "New Zone".
 ![Conf8](./images/win18.png) | Machen sie wieder eine Primary Zone.
 ![Conf9](./images/win19.png) | Da wir keine IPv6 benutzen erstellen wir eine IPv4 Lookup Zone.
 ![Conf10](./images/win20.png) | Geben sie ihre Network-ID ein.
 ![Conf11](./images/win21.png) | Übernehmen Sie de Standard Einstellungen.
 ![Conf12](./images/win22.png) | Erlauben Sie wieder keine Dynamischen Updates.

 >Die Konfiguration kann jetzt nochmals Überprüft und dann abgeschlossen werden.

## Finale Konfigurationen

### Firewall Deaktivieren

Deaktivieren sie noch die Firewall. Dies gilt zur sauberen Kommunikation zwischen ihrem Client und Server.

Gehen Sie dafür beim Client und Server in ihre Firewall einstellungen.
Geben sie dafür einfach in der Suchleiste "Defender" ein und klicken sie auf "Windows Defender Firewall.

![firewall1](./images/wall1.png)

Klicken sie jetzt auf "Inbound Rules" und suchen sie dann die "File and Printer Sharing (Echo Request - ICMPv4-In)".
Machen sie darauf einen REchtsklick und klicken sie dann auf Properties.

![firewall2](./images/wall2.png)

Wählen sie bei den Properties das "Advanced" Tab aus.
Beim Profile bei allen Kästchen einen hacken setzen.

![firewall3](./images/wall3.png)

Aktivieren sie dan ihre neue Regel.

![firewall4](./images/wall4.png)

### Host und Mail Exchanger Hinzufügen

#### Host Hinzufügen

Wir müssen noch einen Host Hinzufügen. Ohne den Host würde der DNS nicht im Netzwerk erreichbar sein.

>Klicken sie mit Rechtsklick auf ihre Forward Lookup Zone die sie erstellt haben und wählen sie "New Host" aus.

![host1](./images/host1.png)

>Geben sie hier einen Namen und eine IP-Adresse ein.
Zusätzlich müssen sie einen Hacken beim Kässtchen für den PTR setzen.

![host2](./images/host2.png)

>Fügen wir noch einen Host für den Client ein.
Gehen sie dafür npochmals mit Rechtsklick auf "New Host".

#### Mail Exchanger Hinzufügen

Jetzt erstellen wir einen Mail Exchanger für ihre DNS-Server um zu Testen ob auch ein Mail Exchanger auf dem Server funktionieren könnte.

>Wieder Rechtsklick auf ihre Forward Lookup Zone die sie erstellt haben. Wählen sie jetzt aber "New Mail Exchanger" aus.

![mx1](./images/host1.png)

>Geben sie ihre Hostdomain bspw. "mail" ein und wählen sie ihren Host den sie erstellt haben als FQDN aus.

![mx2](./images/host3.png)

#### Überprüfung der "Reverse Lookup Zone"

Jetzt müssen wir die Reverse Lookup Zones Überprüfen.

Gehen sie dafür auf die Reverse Lookup Zones und Überprüfen sie ob beide Hosts einen Pointer Hinterlegt haben.

![proof1](./images/proof1.png)

Falls die Pointer nicht hinterlegt worden sind können Sie einen Pointer einfach Hinzufügen.

Machen Sie einen Rechtsklick auf ihre Reverse Lookup Zone und klicken sie dan auf "New Pointer" und geben sie einfach die Benötigten Daten ein.

![proof2](./images/proof2.png)

Beispiel für meinen Client Pointer:

![proof3](./images/proof3.png)

### Letzte Schritte

#### Internet für DNS

Geben sie ihrem DNS einen Zweiten Netzwerkadapter mit NAT. Damit kann ihr DNS auch mit dem Internet Kommunizieren.

![nat1](./images/win24.png)

Ihr DNS Server sollte jetzt eine Internetverbindung haben.

![nat2](./images/win25.png)

#### DNS Konfiguration auf dem Client

Öffnen sie die Netzwerkeinstellungen und ändern sie vom NAT Netzwerkadapter  die DNS einstellungen wie folgt:
**Wichtig**: *Benutzen sie ihre eigene Address Einstellungen für den Preferred DNS server.*
**Info**: *Die Adresse 8.8.8.8 ist für den Google DNS, damit der Client auch ins INternet kann.*

![Client1](./images/test2.png)

Für den M117 Host-Only Netzwerkadapter ändern sie die DNS EInstellungen wie folgt:

![Client2](./images/test3.png)

### Forwarders Konfigurieren

Konfigurieren wir noch den Forwarder für den DNS.

Machen sie einen Rechtsklick auf Ihren Server beim DNS Manager

Klicken sie danach auf "Properties"

![forward1](./images/forward1.png)

Gehen sie ins "Forwarders" Tab und dann auf "edit".

Geben sie hier die IP-Addresse ihres Ziel Servers ein.

Dies ist dazu da das ihr DNS Anfragen die er nicht bearbeiten kann weiter auf ainen anderen Server senden kann.

In diesem fall habe ich den Google DNS benutzt.

![forward2](./images/forward2.png)

## Testing

### DNS Sefltest

Jetzt müssen wir noch einen Selbstest durchführen um zu Testen ob die Konfiguration Korrekt durchgeführt wurde.

Als erstes muss CMD/Powershell geöffnet werden und Folgen commands eingegeben werden:

>- nslookup [Ihr Host]
>- nslookup [Ihr Mailexchanger]
>- nslookup [IP des Clients]
>- Ping [IP des Clients]

Der output sollten wie in diesem Beispielbild sein:

![DNSTest](./images/test1.png)

Wir machen noch einen einzelnen Test für das "Request Forwarding".

Geben sie dafür diesen command ein:

>- nslookup [Website]

![DNSForwardTest](./images/test5.png)

Damit wurde der DNS erfolgreich getestet.

### DNS Client Test

Lassen sie ihren DNS angeschalten für den Client Test.
Starten sie Ihren Client.
Stellen Sie sicher das der Client Zwei Netzwerkadapter haben wo der eine das M177 Host-Only Lab Netzwerk ist.

#### DNS Testen

Jetzt müssen wir die DNS Verbindung vom Client Testen.

Dies machen wir wieder in CMD/Powershell geben sie hierfür diese commands ein:

>- nslookup [Ihr Host]
>- nslookup [Ihr Mailexchanger]
>- nslookup [DNS IP-Addresse]

Der output sollten wie in diesem Beispielbild sein:

![Client3](./images/test4.png)

Wenn sie alles korrekt Konfiguriert haben sollten sie Jetzt einen Funktionierenden DNS Server besitzen.
