# M123 ePortfolio

>Geschrieben von Aaron Sigrist

[Return to Main](/README.md)

[TOC]

## Vorbereitung

Sie sollten diese Sachen schon haben:

- Funktionierende Ubuntu Server VM mit statischer IP im Lab Netzwerk.

- Windows Client VM mit statischer IP im Lab Netzwerk

### SMB Installieren

Als Erstes INstallieren wir den Server.
Geben sie in dieser Reihenfolge die Commands ein.

> sudo apt update
\
> sudo apt install samba
\
> sudo systemctl status smbd

Dies soll der Output sein vom Status Command sein:

![output1](./images/output1.png)

## Konfiguration des Servers

Geben sie *sudo ufw allow 'samba'*

Dies deaktiviert die Firewall für smaba.

Jetzt müssen wir die Config File editieren.

Machen sie zuerst ein Backup mit:
>sudo cp /etc/samba/smb.conf{,.backup}

Dann müssen sie ins config file.

Geben sie diesen Command ein:

>sudo nano /etc/samba/smb.conf

Editieren sie die Config bei diesen Punkten so:

Ändern sie die IP-adresse:

![conf1](./images/conf1.png)

Setzen sie "obey pam restriction" auf no

![conf2](./images/conf2.png)

Teste die Config jetzt mit:

>testparm

Wenn keine Fehler im der Config sind sollte
*Load Service file OK*

Starten sie den dienst mit:
*sudo systemctl restart smbd*

## Directory und User erstellen

### Main Directory Erstellen

Gebe diese Commands ein:

> sudo mkdir /samba
\
> sudo chgrp sambashare /samba
\
> sudo mkdir /samba/shared
\
> sudo chmod 777 /samba/shared

### User erstellen mit directory

Gebe dies ein um einen User mit dem namen Josh zu erstellen.

>sudo useradd -M -d /samba/josh -s /usr/sbin/nologin -G sambashare josh

Dieser wird keine admin Rechte besitzen.

Gebe jetzt diese Commands ein:

>sudo mkdir /samba/josh
\
>sudo chmod 777 /samba/josh
\
>sudo smbpasswd -a josh

Der Output sollte wie folgt sein:

![output2](./images/output2.png)

Hie müssen sie ein neues Passwort für ihren USer eingeben.
Um den User zu aktivieren geben sie diesen command ein:
>sudo smbpasswd -e josh

Es sollte:
>Enabled user josh

## Sambashares Konfigurieren

Geben sie wieder dies ein:
> sudo nano /etc/samba/smb.conf

Jetzt müssen sie ihre share ordner konfigurieren.

Hängen sie dafür im config file dies an:

Beispiel:

![conf3](./images/conf3.png)

Die [shared] konfiguration ist für den allgemeinen "shared" ordner.
Die [josh] & [max] konfigurationen sind für die einzelnen Benutzer.

Speichern und Schliessen sie das config file und geben diese commands ein:

> sudo systemctl restart smbd
\
> sudo systemctl restart nmbd

Jetzt läuft ihr filesharing dienst.

## Testen

Jetzt Testen wir den filesharing dienst auf dem Windows Client.

Starten sie den Client und öffnen sie den File Explorer und gehen sie zu "This PC"
Machen sie einen Rechtsklick und klicken sie dann auf "Add a nwtwork location".

![test1](./images/test1.png)

Gehen sie weiter bis sie ihre Sharefolder-Addresse eingeben müssen.

Geben sie dann ihre Sharefolder-Addresse ein.
In meinem Fall ist sie wie im folgendem Bild:

![test2](./images/test2.png)

Jetzt können sie noch ihrer Ordner bennenen.
Dies ist ihre Persönliche Umbennenung, die nur für sie Sichtbar ist.

![test3](./images/test3.png)

Klicken sie auf "Next" und dann auf "Finish".
Geben sie dann ihre Bneutzerdaten ein vom USer den sie im Server erstellt haben.

Wenn sie alles Richtig gemacht haben sollten sie jetzt Zugriff euf den Ordner haben und ihn auch bearbeiten können.
