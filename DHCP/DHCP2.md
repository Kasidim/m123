# M123 ePortfolio

>Geschrieben von Aaron Sigrist

[Return to Main](/README.md)

[TOC]

## Auftrag 2

## Laborübung 1 - DHCP mit Cisco Packet Tracer

### Router-Konfiguration auslesen

#### Fragen zur Konfiguration

>Für welches Subnetz ist der DHCP Server aktiv?

- Die Subnetzmaske ist 255.255.255.0

>Welche IP-Adressen ist vergeben und an welche MAC-Adressen? (Antwort als Tabelle)

|IP Adresse | Mac-Adresse |
|-----------|-------------|
| 192.168.29.25 | 0001.632C.3508 |
| 192.168.29.24 | 0009.7CA6.4CE3 |
| 192.168.29.26 | 0050.0F4E.1D82 |
| 192.168.29.28 | 0007.ECB6.4534 |
| 192.168.29.27 | 00E0.8F4E.65AA |
| 192.168.29.29 | 00D0.BC52.B29B |

>In welchem Range vergibt der DHCP-Server IPv4 Adressen?

- IPs werden in der Range von: 192.168.29.1 - 192.168.29.254 vergebebn.

>Was hat die Konfiguration ip dhcp excluded-address zur Folge?

- Die IPs von der Range 192.168.29.1-23 und 192.168.19.161-238 können nicht vom DHCP vergeben werden.

>Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?

- Der DHCP kann bis zu 252 IP-Adressen vergeben

### DORA - DHCP Lease beobachten

#### Fragen zum Lease beobachten

>Welcher OP-Code hat der DHCP-Offer?

- DHCP: Op Code (op) = 2 (0x2)

>Welcher OP-Code hat der DHCP-Request?

- DHCP: Op Code (op) = 1 (0x1)
  
>Welcher OP-Code hat der DHCP-Acknowledge?

- DHCP: Op Code (op) = 2 (0x2)

>An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?

- Der Discover wird an die 255.255.255.255 Adresse gesendet. Dies ist die Broadcast-Adresse.

>An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell?

- Der Discover wird an die FF.FF.FF.FF.FF.FF MAC-Adresse gesendet. Dies ist die Bradcast MAC-Adresse

>Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?

- Da der Discover ein Broadcast ist und ein Switch, in den meisten fällen nur Daten Transferiert kann.

>Gibt es ein DHCP-Relay in diesem Netzwerk? Wenn ja, welches Gerät ist es? Wenn nein, wie kann das mithilfe der DHCP-PDUs festgestellt werden?

- k/a

>Welche IPv4-Adresse wird dem Client zugewiesen?

- Die IP 192.168.29.26 wird an das Gerät vergeben. Dies ist die erste Freie IP-Adresse die der DHCP vergeben kann.

#### Screeenshots der PDUS

- Screenshot des DHCP-Discovers PDUs

![DHCPDISCOVER](images/Screen1.png)

- Screenshot des DHCP-Offers PDUs

![DHCPOFFER](images/Screen2.png)

- Screenshot des DHCP-Request PDUs

![DHCPREQUEST](images/Screen3.png)

- Screenshot des DHCP-Acknowledge PDUs

![DHCPACKNOWNLEDGE](images/Screen4.png)

### Netzwerk umkonfigurieren

- Screenshot von der IPv4 einstellung.

![IPv4config](images/Shot1.png)

- Screenshot von dem Default-Gateway

![IPv4Gateway](images/Shot2.png)

- Screenshot der Website des Servers

![Webserver-Website](images/Shot3.png)
