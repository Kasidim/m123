# M123 ePortfolio

>Geschrieben von Aaron Sigrist

[Return to Main](/README.md)

[TOC]

## Auftrag 1

## DHCP Konfiguration

### Einleitung

In dieser Dokumentation wird erklärt wie sie Filius einen **DHCP-Server** einrichtet. Dies geschiet in einer Stern-Topologie. Wir werden alle Client IP-Adressen über de  **DHCP-Server** verwaltet.

---

### DHCP Setup

Bauen sie zuerst ihr **DHCP** Architektur auf.

Bei dieser Architektur werden wir Zwei Clients mit **Dynamischen** und einen Client mit einer **Statischen** IP-Adressen einrichten. Sie brauchen neben den 3 Clients ausserdem eine  **DHCP**-Server und einen switch.

Die Addresszuweisung ist wie Folgt:

| Geräte | IP-Adresse | Adress art |
| ----------- | ----------- | ----------- |
| DHCP Server | 192.168.10 | Statisch |
| Client 1 | 192.168.0.150-170 | Dynamisch |
| Client 2 | 192.168.0.150-170 | Dynamisch |
| Client 3 | 192.168.0.50 | Statisch |

![Architecture](./images/1.png)

#### DHCP Grundeinstellung

Klicken sie auf ihren   **DHCP-Server** und dann auf  **DHCP-Server einrichten**.

![Button](./images/2.png)

Konfigurieren sie ihren **DHCP** wie im Bild.
Klicken sie anschliessend auf  *DHCP aktivieren*.

Diese Einstellungen sind für die 2 Clients mit **Dynamischen** IP-Adressen.
Drücken sie nachdem sie die Grundeinstellung abgeschlossen haben auf *OK*.

![Grundeinstellung](./images/3.png)

#### DHCP Statische Adresszuweisung

Wenn sie die Grundeinstellung eingerichtet haben müssen sie noch die Statische IP-Zuweisung Einrichten.

Öffnen sie das Fenster nochmals wo sie die Grundeinstellungen gemacht haben und klicken sie auf **Statische Adresszuweisung**.

Geben sie hier dann die MAC-Adresse von ihrem Gerät ein, in diesem Fall ist die MAC-Adresse **49:11:5C:DA:77:23**.
Geben sie ausserdem die nötige IP-Adresse ein, in diesem Fall: **192.168.0.50**.

Drücken sie danach auf *Hinzufügen*.

![Statische-Einstellung](./images/4.png)

### DHCP Aktivieren & Testen

#### Aktivieren des DHCP bei Clients

Schliessen sie di  **DHCP** Konfiguration und Aktivieren sie bei ihren Clients di  **DHCP** KonfiSuration. (Siehe Bild)

![Anwenden](./images/5.png)

Überprüfen sie ausserdem die Statische und die Dynamischen IP-Adressen.

#### Testen der Konfiguration

Öffnen sie bei einem Ihrer Clients die Befehlszeile und machen sie einen *Ping* Command.
*Pingen* sie ein anderes Gerät wo sie DHCP eingerichtet haben.

![Testing](./images/6.png)

Sollten die *Ping* commands ohne Probleme Funktionieren haben sie alles korrekt eingestellt.

 :clap: Kongratulation :clap:
 Sie haben einen DHCP-Server in Filius eingerichtet.
