# M123 ePortfolio

>Geschrieben von Aaron Sigrist

[Return to Main](/README.md)

[TOC]

## Auftrag 3 DHCP Dienst zur Verfügung stellen

### Vorbereitung

- Sie sollten einen VM-Client mit dem zweitem Netzwerkadapter vom Modul 117 bereitgestellt haben.
- Falls kein vorhander Client vom Modul 117 erstellen sie einen neuen CLient bis zum schritt der IP-adressen vergabe vom Modul 117.

### VM erstellen

- Lade die Server Iso von Ubuntu Server Herunter.
- Erstellen Sie eine Neue VM in Vmware. Sie können das ISO Image schon bei der Erstellug der VM hinufügen.
- Fügen Sie einen zweiten Netzwerkadapter hinzu und konfigurieren Sie das Netzwerk auf das schon ertsellte vom Modul M117.

Die Konfiguration sollte wie folgt aussehen:

![Config](./images/S0.png)

- Starten Sie die VM.

#### Server Installieren

>Starten Sie die VM und drücken Sie auf "Try or Install Ubuntu Server"

![Start-Intall](./images/S1.png)

>Laden Sie das Installer Update herunten.

![Installer-Update](./images/S2.png)

>**Ändern Sie ihre IP Adresse nicht**
>Überspringen Sie die Proxy-Adresse.
>Warten Sie dann auf den "Mirror-Test".

![Mirror-Test](./images/S3.png)

>Folgen Sie danach Schritten auf dem Bildschirm.

- Konfigurieren Sie nichts neu.
- Benutzernamen und Kennwort setzen.

![Profile](./images/S4.png)

>Die Installation weiterführen bis Sie die VM neustarten müssen. **Nichts ändern.** Starten Sie die VM dan neu.

### DHCP Ausetzen

#### Updaten des Servers

Geben Sie als Erstes *sudo apt-get update* ein um den Server zu Aktualiesieren.

Geben Sie *sudo apt install isc-dhcp-server -y* ein und warten Sie auf die Installation.

#### Konfiguration

>Geben Sie "ip a" ein und suchen Sie ihren sekundären Netzwerkadapter, sie ha wahrscheinlich keine IP-Adresse. In diesem fall ist der Name "ens24".
*Wichtig: Ihr Netzwerkadapter könnte einen anderen Namen haben*

![network-adapter](./images/S5.png)

> Geben Sie den Command *sudo nano /etc/default/isc-dhcp-server/* ein. Und tragen Sie in den Gänsefüsen bei "Interfacesv4" denn namen Ihrer Netzwerkkarte.

![interface](./images/S6.png)

>- Geben Sie *sudo nano /etc/dhcp/dhcpd.conf* ein und ändern Sie Folgendes:
>- Entfernen Sie das # bei "authoritative;"
>- subnet und range auf ihre Konfiguration.

![Config](./images/S7.png)

Speichern und Schliessen sie jetzt das config file.

#### Statische IP für den DHCP Konfigurieren

Jetzt braucht der DHCP eine feste IP-Adresse.
Geben Sie als erstes *cd /etc/netplan/* ein um in das Netplan Config File verzeichnis zu gelangen.
Damit sollten sie jetzt im Netplan verziechnis sein.

Es sollte etwa so aussehen:

![Verzeichnis](./images/S8.png)

Nun müsses sie die das Config File öffnen
geben sie dafür *sudo nano /etc/netplan/00-installer-config.yaml* ein.
Damit sollte das Config File geöffnet sein.

Konfigurieren sie das File mit der Adresse die sie ihrem DHCP Server geben wollen:
*Info: Bei mir heisst der Netzwerkadapter "ens34" er könnte bei ihnen anders heissen.*

![StaticIP](./images/S9.png)

### Starten und Testen

#### DHCP Starten

Geben sie diese Commands in dieser Reihenfolge ein.

> sudo systemctl start isc-dhcp-server(Für das Starten des Dienstes)
> sudo systemctl enable isc-dhcp-server(Für das Aktivieren des Dienstes)

Um zu überprüfen ob der Dients läuft geben sie diesen command ein:
>$ sudo systemctl status isc-dhcp-server

Es sollte ähnlich wie in diesem Bild sein:
Falls dies so ist und der DHCP "active(running)" ist haben sie alles korrekt konfiguriert.

![DHCPstatus](./images/S10.png)

#### DHCP auf Client Aktivieren

Beenden sie den DHCP server nicht und etarten sie einen Client.

Öffnen sie im Client die "Network Connections" in den Einstellungen und Klicken sie auf "Ethernet1".

![ConnectionSettings](./images/S11.png)

Klicken sie im neuen Fester auf "Properties" um den DHCP auf diesem Gerät zu aktivieren.

![EthernetStatus](./images/S12.png)

Im jetzigen Fenster müsses sie auf "Internet Protocol Version 4" klicken.
Aktivieren sie den DHCP indem sie "Obtain an IP adress automatically" anwählen.

Es sollte wie in diesem Bild aussehen:

![EnableDHCP](./images/S13.png)

#### Testen Client

Geben sie im im CMD des Client *ipconfig /all* ein und überprüfen sie die einstellung.
Sie sollten bei ihrem zweitem Netzwerkadapter eine DHCP IP-Adresse erhalten wie auch eine Lease Time.

![TestFull](./images/S14.png)

![TestIP](./images/S15.png)

Wenn alle Einstellungen Korrekt sind sollte ihr ipconfig ähnlichkeit haben wie die im Beispielbild.

#### Testen Server

Geben Sie im Server *sudo cat /var/lib/dhcp/dhcpd.leases* ein.

Wenn das Lease vergeben worden ist sollte es ähnlichkeiten mit diesem Beispiel haben:

![TestLease](./images/S16.png)

Danke für de 7.0
