# M123 ePortfolio

Dies ist das Portfolio von Aaron Sigrist für das Modul 123.
In diesem Modul behandeln wir bestimmte Serverdienste.

Untenstehen sind Alle Aufträge Dokumentiert.

## Inhaltsverzeichniss

[DHCP](./DHCP/)

- [Auftrag 1](./DHCP/DHCP1.md)

- [Auftrag 2](./DHCP/DHCP2.md)

- [Auftrag 3](./DHCP/DHCP3.md)

[DNS](./DNS/)

- [DNS-Server in Windows](./DNS/README.md)

[SMB](./SMB/)

- [SMB auf Linux Server](./SMB/README.md)

[Print-Server](./Print-Server/)

- [Print-Server in Windows](./Print-Server/README.md)

[Splendid Blur AG](./Splendid-Blur-AG)

- [Netzwerk für ein KMU einrichten](./Splendid-Blur-AG/README.md)
