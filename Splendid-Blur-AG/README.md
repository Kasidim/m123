# M123 ePortfolio

>Geschrieben von Aaron Sigrist

[Return to Main](/README.md)

[TOC]

## Einleitung

Dies ist meine Dokumentation wie ich nach Auftrag ein KMU im Filius aufgesetzt habe.
Ich werde nicht jeden schritt denn ich gemacht habe hineinschreiben sondern nur den Ablauff meiner Arbeit.

### Vorbereitung

Als erstes habe ich die Filius Datei geöffnet die Geräte verbunden und die IP-Ranges wie auch der Websitename vom Webserver festgehalten.

Dies dient zur vereinfachten Konfiguration der Geräte.

Ausserdem habe ich sie schon bennant um verwechslungen entgegen zu wirken.

| Before | After |
| - | - |
| ![img1](./images/img1.png) | ![img2](./images/img2.png) |

## SW01 Netzwerk - DHCP & 6 Clients

Als erstes habe ich die sechs Clients mit dem DHCP-Server eingerichtet.
Dies sind die IP-Einstellungen des DHCP-Servers:

![img3](./images/img3.png)

Danach habe ich nach der Vorgegebenen Netzmaske den DHCP-Server eingerichtet:

![img4](./images/img4.png)

Da einer der Sechs Clients eine Statische IP braucht habe ich den Client PC11 mit der MAC-Addresse eine Statische IP durch den DHCP-Server vergeben.

So sieht es dann aus:

![img5](./images/img5.png)

Danach habe ich die Clients umgestellt so das sie ihre IP Informationen über den DHCP beziehen.

![img6](./images/img6.png)

## SW03 Netzwerk - 2 Clients mit Statischer IP

Als zweites habe ich die beiden Clients beim SW03 Switch eingerichtet.

| PC51 | PC52 |
| - | - |
|![img7](./images/img7.png) |![img8](./images/img8.png) |

Hier müsste man nach vorgabe nicht mehr viel machen.

## SW02 Netzwerk - Web- & DNS-Server

Zuerst habe ich den Servern die IP Einstellungen eingerichtet.

| DNS-Server | Webserver |
| - | - |
|![img9](./images/img9.png) |![img10](./images/img10.png) |

### Zwischentest ping command

Bevor ich anfange die Beiden Server einzurichten machen wir noch ein paar ping Tests.

Die mache ich von einem DHCP Client aus um gleichzeitig zu Testen ob der DHCP auch sauber Läuft.

![img11](./images/img11.png)

Da ich in jedem Netzwerk ein Gerät anpingen kann sollte alles in ordnung sein.

### Web- & DNS-Server einrichten

Als erstes starten wir den Webserver. Wir müsses da wir es im Filius  machen, keine Website erstellen sondern nur den Dienst hinzufügen und starten.

Das macht man so:

>**1.**
>Webserver bei "Software-Installationen" installieren und öffnen.
>
>![img12](./images/img12.png)
\
>**2.**
>Auf "Starten" Drücken
>
>![img13](./images/img13.png)
>
>Damit ist der Webserver Gestartet.

Jetzt müssen wir den DNS einrichten.
Ich habe bei mir die Webaddresse auf "splendid-blur.ch" mit der IP-Addresse 172.16.100.4 für meine Website benutzt.

Füge Ausserdem PC11, PC51 & PC52 Hinzu.

![img14](./images/img14.png)

Starten sie nach dem sie eine Addresse HInzugefügt haben denn Server und setzen sie das Häckchen.

### Web- & DNS-Server Testen

Um die beiden Server zu Testen müssen wir jetzt auf einen Client.
Da ich alle Clients Korrekt eingerichtet habe sollte jeder Client für die Tests Funktionieren.

Hier ist das Ergebniss von meinem versuch die Website Adresse einzugeben.

![img15](./images/img15.png)

Ich habe die Website selbst ein Wenig angepasst.

## Mögliche Änderung

Ich hätte zur vereinfachung die IP Aressen durch einen DHCP Verwalten lassen. Anstelle von mehreren Netzwerken.
Falls mehrere Netzwerke Nötig sind Trotzdem bis auf den Serverraum alle IP Address Informationen durch DHCP zu verwalten.

Alle Clients die dann eine Statische IP benötigen können wir über den DHCP Hinzufügen. Damit müssen wir nicht einzelnen Clients Lokal eine Statische vergeben.

Hier ein mögliches Beispiel:

![img16](./images/img16.png)
